
#pragma once

#include <string>
#include <iostream>

#include "aku.hpp"
#include "animal.hpp"
#include "animalfactory.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nene.hpp"
#include "nunu.hpp"
#include "palila.hpp"

using namespace std;


namespace animalfarm {
   string                     generateRandomName();
   enum animalfarm::Color     generateRandomColor();
   enum animalfarm::Gender    generateRandomGender();
   bool                       generateRandomBool();
   float                      generateRandomWeight();
   float                      generateRandomTemp();
   string                     generateRandomLocation();
   char                       randomUppercaseLetter();
   char                       randomLowercaseLetter();
}
