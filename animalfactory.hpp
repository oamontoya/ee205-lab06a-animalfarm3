#pragma once

#include "utility.hpp"

using namespace std;

namespace animalfarm {

   class AnimalFactory {
      public:
         static Animal* getRandomAnimal();
   };
}

