///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file dog.hpp
/// @version 1.0
///
/// Exports data about all dogs
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   10_FEB_2021
///////////////////////////////////////////////////////////////////////////////


#pragma once

#include <string>

#include "mammal.hpp"

using namespace std;

namespace animalfarm {

   class Dog : public Mammal {
   public:
	   Dog( string newName, enum Color newColor, enum Gender newGender );
   
      string name;
	
	   void printInfo();
	   virtual const string speak();
   };
}
