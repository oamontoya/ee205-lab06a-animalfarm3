###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 05a - Animal Farm 2
#
# @file    Makefile
# @version 1.0
#
# @author Osiel Montoya <montoyao@hawaii.edu>
# @brief  Lab 05a - Animal Farm 2 - EE 205 - Spr 2021
# @date   10_FEB_2021
###############################################################################

all: main

test: utility

utility.o: utility.hpp utility.cpp
	g++ -c utility.cpp

main.o:  animal.hpp main.cpp
	g++ -c main.cpp

animal.o: animal.hpp animal.cpp
	g++ -c animal.cpp

animalfactory.o: animalfactory.hpp animalfactory.cpp
	g++ -c animalfactory.cpp

mammal.o: mammal.hpp mammal.cpp
	g++ -c mammal.cpp

cat.o: cat.cpp cat.hpp
	g++ -c cat.cpp

dog.o: dog.cpp dog.hpp
	g++ -c dog.cpp

fish.o: fish.cpp fish.hpp
	g++ -c fish.cpp

nunu.o: nunu.cpp nunu.hpp
	g++ -c nunu.cpp

aku.o: aku.cpp aku.hpp
	g++ -c aku.cpp

bird.o: bird.cpp bird.hpp
	g++ -c bird.cpp

palila.o: palila.cpp palila.hpp
	g++ -c palila.cpp

nene.o: nene.cpp nene.hpp
	g++ -c nene.cpp

main: main.cpp  main.o animal.o mammal.o cat.o dog.o fish.o nunu.o aku.o bird.o palila.o nene.o utility.o animalfactory.o
	g++ -o main main.o animal.o mammal.o cat.o dog.o fish.o nunu.o aku.o bird.o palila.o nene.o utility.o animalfactory.o

utility: utility.cpp utility.o mammal.o fish.o bird.o aku.o animal.o animalfactory.o cat.o dog.o nene.o nunu.o palila.o
	g++ -o utility utility.o mammal.o fish.o bird.o aku.o animal.o animalfactory.o cat.o dog.o nene.o nunu.o palila.o

clean:
	rm -f *.o main utility
